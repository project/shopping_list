CONTENTS OF THIS FILE
---------------------
   
 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
 * Usage
 
 INTRODUCTION
------------

This module will help user to add products of an E-commerce website to
"Shopping List".

REQUIREMENTS
------------
- ctools

INSTALLATION
------------
* Go to the Modules page (/admin/modules) and enable it.

CONFIGURATION
-------------
* Go to "Shopping List configuration" page (admin/config/set-shopping-list).
->Enter the limit of shopping lists to be added.

USAGE
-----
This module allows user to add a product to shopping list by creating a simple 
link or button.

i.e. Link can be created as 
l(t('Add to Shopping List'), "add-to-shopping-list/" . % . "/nojs", array('attributes' => array('class' => 'ctools-use-modal')))
 where % is product id which needs to be passed dynamically for a product to be
added in a list.

You can create a new shopping list by clicking on "Create New List" button.
List of Shopping lists will be displayed choose one or more lists where you want
to add the product in. Click on Apply to add a product to a list

Go to (/shopping-lists) to outlook the shopping lists created.
There are "view" and "delete" links associated with each list:
-> View link: To check the products added in a particular shopping list.
There are "add to cart" and "delete" links corresponding to the product name
to add a particular product into cart and delete the product from the cart 
respectively. If you want to add all the products of the list check all product
and click "ADD TO CART" in the similar way you can delete all the product from
the list by clicking "DELETE ALL".

->Delete link: Deletes a particular Shopping List.

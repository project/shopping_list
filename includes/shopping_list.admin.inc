<?php

/**
 * @file
 * Administration page for Shopping List module.
 */

/**
 * Sets limit for number of lists to be added.
 */
function shopping_list_set_no_of_lists() {
  $form['shopping_list_no_of_list'] = array(
    '#type' => 'textfield',
    '#title' => t('Number of list'),
    '#description' => t('Enter number of Lists'),
    '#default_value' => variable_get('shopping_list_no_of_list', ''),
  );
  return system_settings_form($form);
}

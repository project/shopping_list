(function ($) {
  'use strict';
  Drupal.behaviors.shopping_list = {
    attach: function (context, settings) {
      $('#shopping-create-list-wrapper').hide();
      $('#create_new_shopping_list').click(function (event) {
        $.noConflict();
        event.preventDefault();
        $('#shopping-create-list-wrapper').toggle();
      });
    }
  };
})(jQuery);
